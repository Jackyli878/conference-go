from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture(city, state):
    params = {"query": f"{city},{state}", "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (IndexError, KeyError):
        return {"picture_url": None}


def get_weather(city, state):
    location_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    location_url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(
        location_url,
        params=location_params,
    )
    location = json.loads(response.content)
    lat = location[0]["lat"]
    lon = location[0]["lon"]
    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather = json.loads(weather_response.content)
    weather_dict = {}

    try:
        weather_dict["temp(F)"] = weather["main"]["temp"]
        weather_dict["Weather Description"] = weather["weather"][0][
            "description"
        ]
    except (KeyError, IndexError) as e:
        print(f"Error retrieving weather data: {e}")
        return None

    return weather_dict
